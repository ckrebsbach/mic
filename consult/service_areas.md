[TOC]

# MIC: Consult Service Areas
- *Current as of 2024.03.14*

## Division Service Area Requests
### Allergy, Pulmonary, and Critical Care Medicine (Lowery)
- **Epic: Pulmonary (1044, 1162, 3055)**
  - Pulmonary Consult
  - Pulmonary Procedure Service (for Thoracentesis, Bronchoscopy, Chest Tubes)
    - No Order Exists
- **Epic: Advanced Pulmonary Service (1081)**
  - APS - Transplant, CF, PH
    - No Order Exists
 - **Epic: Interventional Pulmonary (3083)**
  - Interventional Pulmonary Service (for Advanced Diagnostic Pulmonary or Pleural Disease)
    - No Order Exists
- **Epic: Critical Care (1069, 1208)**
  - Critical Care
- **Epic: Consult Adult Allergy (3002)**
  - Allergy Consult

### Cardiovascular Medicine
- **Epic: Cardiovascular Medicine**

### Endocrinology, Diabetes, and Metabolism (Dhital)
- **Epic: Diabetes Management (3014)**
  - Diabetes Management Services (staffed by APPs)
- **Epic: Endocrine (3016)**
  - Endocrine Consult Service

### Gastroenterology and Hepatology (Austin)
- **Epic: Gastroenterology (1009, 1190, 1221, 3019)**
  - General GI
  - Interventional Endoscopy (ERCP/EUS)
- **Epic: Hepatology (3022)**
  - Hepatology

### General Internal Medicine
- **Epic: Internal Medicine**

### Geriatrics and Gerontology (Chapman)
- **Epic: ACE/Geriatrics (3001)**
  - ACE/Geriatrics

### Hematology, Medical Oncology, and Palliative Care (Kyriakopoulos)
- **Epic: Oncology (1022, 1194)**
  - UW Oncology Consults
- **Epic: Hematology (1014, 1192, 3310)**
  - UW Hematology Consults
- **Epic: Palliative Care (1080)**
  - UW Palliative Consults
- Separate Group of Items for Each of the above?
  - Meriter Heme/Onc Consults (Data Access Concerns, Palliative as well?)

### Hospital Medicine

### Infectious Disease (Marshall, Saddler)
- **Epic: Infectious Disease (1105, 3023)**
  - UW1 - Gen Med, Hospitalist, Family Med, Gen Surgery, Critical Care (med and
    surg), Oncology, Thoracic Surgery, Cardiac Surgery, and Emergency
  - UW2 - Pre-transplant Eval, Transplant, Hematology
  - UW3/VA - Urology, Vascular, Ortho, Plastics, ENT, EMH, Neurosurgery, Optho,
    Burn, Wound, and Gyn/Oncology (only gyn/onc), Rehab Hospital Consults, VA
  - UW4 (Faculty Only Service) - Access Center, Urgent Outpatient, Urgent Care,
    Rehab Hospital Questions (not related to a current patient), Special
    Pathogens, Clinic Triage
  - Meriter/Select - All Consults (Data Access Concerns)
- **Epic: Infection Control (3050)**
  - Infection Control

### Nephrology (Norby)
- **Epic: Nephrology (3082)**
  - Acutes Consult Service at UWH
  - EMH Nephrology Consult Service (EMH only)
- **Epic: Renal-End Stage (3037)**
  - ESRD Consult Service at UWH
- **Epic: Renal Transplant?, Pancreas Transplant?, Liver Transplant?**
  - Transplant Nephrology Consult Service at UWH (or Nephrology still?)
- **Epic: Transplant Medicine (3103)**
  - Medical Transplant Inpatient Service at UWH

### Rheumatology (Campbell)
- **Epic: Rheumatology (1141, 3039)**
  - Adult Rheumatology
- **Epic: Pediatric Rheumatology (1076)**
  - Pediatric Rheumatology

## Patient Selection Flowchart
```mermaid
classDiagram

class ADT_SERV_AREA_ID["Service Area"]
ADT_SERV_AREA_ID : UW Health

class HOSP_ADMSN_TIME["Hospital Admission"]
HOSP_ADMSN_TIME : min 20230301
HOSP_ADMSN_TIME : max 20240301

class ADT_PAT_CLASS_C["PATIENT CLASS"]
ADT_PAT_CLASS_C : Emergency
ADT_PAT_CLASS_C : Inpatient
ADT_PAT_CLASS_C : Observation
ADT_PAT_CLASS_C : Outpatient Short Stay
ADT_PAT_CLASS_C : First Day Surgery
ADT_PAT_CLASS_C : Surgical Admit

class NOTE_STATUS_C["NOTE STATUS"]
NOTE_STATUS_C : Incomplete
NOTE_STATUS_C : Signed
NOTE_STATUS_C : Addendum
NOTE_STATUS_C : Revised
NOTE_STATUS_C : Cosigned
NOTE_STATUS_C : Finalized
NOTE_STATUS_C : Unsigned
NOTE_STATUS_C : Cosign Needed
NOTE_STATUS_C : Incomplete Revision
NOTE_STATUS_C : Cosign Needed Addendum
NOTE_STATUS_C : Shared

ADT_SERV_AREA_ID --> HOSP_ADMSN_TIME
HOSP_ADMSN_TIME --> ADT_PAT_CLASS_C
ADT_PAT_CLASS_C --> NOTE_STATUS_C
```

## Frequency Distributions for Select Rows
- one row for:
  - specific patient and stay
  - first note of each day
  - multiple service areas included
  - any others? provider id? note type?

### Hospital
UW Health = **46620**

### Consult Note Month and Date
- Not hospital admission time
- Should notes be constrained to during the inpatient stay?
20230301 to 20240229

year | month | n
----:|------:|-----:
2023 | 06    | 4076
2023 | 05    | 4054
2023 | 08    | 3981
2023 | 12    | 3959
2024 | 01    | 3929
2023 | 09    | 3907
2023 | 04    | 3862
2023 | 11    | 3841
2023 | 07    | 3828
2024 | 10    | 3814
2023 | 03    | 3738
2024 | 02    | 3631

### Note Type (IP_NOTE_TYPE_C)
code   | note type         | n
------:|:------------------|-----:
100015 | Consult Follow-up | 29443
100004 | Consult Note      | 17177
2      | Consults          |     0

### Note Status (NOTE_STATUS_C)
code | note status            | n
:----|:-----------------------|-----:
2    | Signed                 | 32564  
3    | Addendum               |  7459
9    | Cosign Needed          |  4997
12   | Shared                 |  1326
11   | Cosign Needed Addendum |   119
10   | Incomplete Revision    |   111
8    | Unsigned               |    22
1    | Incomplete             |    22
5    | Revised                |     0
6    | Cosigned               |     0
7    | Finalized              |     0

### Author Service Area (AUTHOR_SERVICE_C)
code | author service area        | n
----:|:---------------------------|-----:
3023 | Infectious Disease         | 10337
3001 | Palliative Care            |  5097
3082 | ACE/Geriatric              |  4654
1080 | Gastroenterology           |  4419
3019 | Nephrology                 |  3427
3055 | Pulmonary                  |  2817
3016 | Endocrine                  |  2774
3103 | Transplant Medicine        |  2393
3022 | Hepatology                 |  2138
3039 | Rheumatology               |  1954
1014 | Hematology                 |  1714
1081 | Advanced Pulmonary Service |  1602
1022 | Oncology                   |  1081
3083 | Interventional Pulmonary   |   489
1069 | Critical Care              |   486
3014 | Diabetes Management        |   475
3002 | Allergy                    |   429
3050 | Infection Control          |   186
1076 | Pediatric Rheumatology     |   147
3037 | Renal-End Stage            |     1

### Patient Class (ADT_PAT_CLASS_C)
code | patient class         | n
----:|:----------------------|-----:
2    | Inpatient             | 45026
3    | Observation           |   734
4    | Outpatient Short Stay |   719
1    | Emergency             |   141
6    | First Day Surgery     |     0
8    | Surgical Admit        |     0

division                                           | count
:--------------------------------------------------|------:
Infectious Disease                                 | 10523
Hematology, Medical Oncology, and Palliative Care  |  7214
Nephrology                                         |  7048
Allergy, Pulmonary, and Critical Care Medicine     |  5823
Gastroenterology and Hepatology                    |  5565
Geriatrics and Gerontology                         |  5097
Endocrinology, Diabetes, and Metabolism            |  3249
Rheumatology                                       |  2101 

### Consult Notes by Month/Division
division            | 2303 | 2304 | 2305 | 2306 | 2307 | 2308
:-------------------|-----:|-----:|-----:|-----:|-----:|-----:
Allergy...          | 603	 | 551  | 515  | 467  | 462  | 449
Endocrinology...    | 243	 | 264  | 275  | 266  | 274  | 290
Gastroenterology... | 464	 | 498	| 441	 | 452	| 475	 | 465
Geriatrics...	      | 362	 | 451	| 421	 | 439	| 412	 | 459
Hematology...       | 594	 | 514	| 605	 | 613	| 530	 | 575
Infectious Disease  | 774	 | 890	| 886	 | 922	| 801	 | 981
Nephrology          | 468	 | 565	| 725	 | 701	| 708	 | 578
Rheumatology        | 230	 | 129	| 186	 | 216	| 166	 | 184

division            | 2309 | 2310 | 2311 | 2312 | 2401 | 2402
:-------------------|-----:|-----:|-----:|-----:|-----:|-----:
Allergy...          | 432  | 525  | 400  | 476  | 519  | 424
Endocrinology...    | 194  | 276  | 314  | 281  | 332  | 240
Gastroenterology... | 504  | 452  | 474  | 473  | 423  | 444
Geriatrics...	      | 400  | 419  | 415  | 485  | 471  | 363
Hematology...       | 708  | 632  | 656  | 659  | 569  | 559
Infectious Disease  | 918  | 879  | 838  | 853  | 875  | 906
Nephrology          | 607  | 530  | 577	 | 561  | 551  | 477
Rheumatology        | 144  | 101  | 167  | 171  | 189  | 218

---

## Frequency Distributions for Defined Parameters
Note: This includes all notes, which can just be addendums to current notes or cosign addendums. Should the original note be counted only one without adding more note volume for updates and addendums? Could billing data be more indicative of a consult?

### Service Area (ADT_SERV_AREA_ID)
- UW Health = **66187**
- No Meriter Data is Included
- Total number of consult notes is **66187**, which includes all addendums and potential incomplete notes. Need to discuss further.

### Hospital Admission Time (HOSP_ADMSN_TIME)
20230301 to 20240229

year | month | n
----:|------:|-----:
2023 | 03    | 6507
2023 | 04    | 6216
2023 | 06    | 6154
2023 | 08    | 5868
2023 | 05    | 5727
2023 | 11    | 5473
2023 | 09    | 5338
2023 | 10    | 5166
2023 | 07    | 5160
2024 | 01    | 5133
2023 | 12    | 5053
2024 | 02    | 4392

### Note Type (IP_NOTE_TYPE_C)
code   | note type         | n
------:|:------------------|-----:
100015 | Consult Follow-up | 41693
100004 | Consult Note      | 24494
2      | Consults          |     0

### Note Status (NOTE_STATUS_C)
code | note status            | n
:----|:-----------------------|-----:
2    | Signed                 | 35171  
3    | Addendum               | 21013
9    | Cosign Needed          |  5454
12   | Shared                 |  2648
11   | Cosign Needed Addendum |  1458
10   | Incomplete Revision    |   390
8    | Unsigned               |    30
1    | Incomplete             |    23
5    | Revised                |     0
6    | Cosigned               |     0
7    | Finalized              |     0

### Author Service Area (AUTHOR_SERVICE_C)
code | author service area        | n
----:|:---------------------------|-----:
3023 | Infectious Disease         | 15266
3001 | Palliative Care            |  6010
1080 | Gastroenterology           |  5124
3082 | ACE/Geriatric              |  7568
3019 | Nephrology                 |  5940
3055 | Pulmonary                  |  4055
3022 | Hepatology                 |  3822
3016 | Endocrine                  |  3747
3039 | Rheumatology               |  3101
3103 | Transplant Medicine        |  2727
1081 | Advanced Pulmonary Service |  2281
1014 | Hematology                 |  2103
1022 | Oncology                   |  1547
1069 | Critical Care              |   696
3002 | Allergy                    |   657
3083 | Interventional Pulmonary   |   617
3014 | Diabetes Management        |   549
3050 | Infection Control          |   193
1076 | Pediatric Rheumatology     |   183
3037 | Renal-End Stage            |     1

### Patient Class (ADT_PAT_CLASS_C)
code | patient class         | n
----:|:----------------------|-----:
2    | Inpatient             | 63961
3    | Observation           |  1048
4    | Outpatient Short Stay |  1001
1    | Emergency             |   177
6    | First Day Surgery     |     0
8    | Surgical Admit        |     0

## Using ALL Note Types
- The table below uses all notes, not constrained to one note per day per patient.
- This would change the options for selecting the first distinct note per patient and service area.

note type                                                 | count
:---------------------------------------------------------|-----:
Progress Notes                                            | 77904
Consult Follow-up                                         | 41909
Consult Note                                              | 24547
H&P                                                       | 15644
Procedures                                                |  9768
Discharge Summary                                         |  6935
Discharge Planning                                        |  1286
Nursing Shift Summary                                     |  1078
Interim Summary                                           |  1027
Miscellaneous                                             |  1020
Conference Note                                           |   399
Operative Note                                            |   331
RN Care Note                                              |   233
Oncology Treatment Modification                           |   210
H&P (View-Only)                                           |    96
RPh Notes                                                 |    60
Result Encounter Note                                     |    57
Operative Note: Brief                                     |    52
Sending / Returning Handoff Note                          |    44
TP Review Notes                                           |    40
RPh Medication Hx                                         |    37
RPh to Pharmacy DC Handoff                                |    36
Nursing Admission Note                                    |    32
Brain Death Note                                          |    31
Handoff                                                   |    29
Code Blue                                                 |    26
Nursing Discharge Note                                    |    23
ACP (Advance Care Planning)                               |    17
Antimicrobial Stewardship                                 |    17
ED Provider Notes                                         |    16
Rapid Response Team                                       |    15
Interval H&P Note                                         |    15
ED Notes                                                  |    12
Attestation                                               |    12
Perioperative Note                                        |     9
ED Provider Sign Out Notes                                |     8
Care Team Visit                                           |     7
Dialysis - mTPE Procedure                                 |     6
Addendum Note                                             |     4
ED Expected Notes                                         |     3
Behavioral Response                                       |     3
Research Note                                             |     3
Tumor Board                                               |     2
H&P (External)                                            |     2
Dialysis - Continuous Renal Replacement Therapy (CRRT)    |     2
Fall Occurrence                                           |     1
Dialysis - Hemodialysis (HD)                              |     1
Behavior Plan Note                                        |     1